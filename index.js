const express = require('express')
const app = express()
const fs = require('fs')

app.use(express.json())

app.post('/inserir', (req, res) => {
    console.log(req.body)
    res.send("OK")
})

app.listen(88, () => {
    console.log("Estou online!")
})

app.get('/gerador', (req, res) => {
    var arquivo = fs.readFileSync('./gerado_jogador.json', 'utf8')
    var objeto = JSON.parse(arquivo)

    numero_nome = Math.floor(Math.random() * 18 + 0)
    numero_sobrenome = Math.floor(Math.random() * 28 + 0)
    numero_posicao = Math.floor(Math.random() * 8 + 0)
    numero_clube = Math.floor(Math.random() * 20 + 0)
    idade = Math.floor(Math.random() * 40 + 17)

    mensagem = objeto.nome[numero_nome] +" "+ objeto.sobrenome[numero_sobrenome] + " é um futebolista brasileiro de "+
               idade +" anos que atua como "+ objeto.posicao[numero_posicao] +". Atualmente defende o "+ objeto.clube[numero_clube]

    res.send(mensagem)
})

/*
app.set('json replacer', (chave, valor) => {
    if(chave == 'senha'){
        return '*'
    } else{
        return valor
    }
})

app.get('/usuarios', (req, res) => {
    var arquivo = fs.readFileSync('./dados.json', 'utf8')

    var objeto = JSON.parse(arquivo)

    //res.send(objeto)
    
    res.json(objeto)
    
    //res.json(objeto.usuarios[0]['usuario'])
    //res.json(objeto.usuarios[0].usuario)
})
*/

/*
var mascara = function(chave, valor){
    if(chave == 'senha'){
        return "******"
    } else {
        return valor
    }
}

var objeto = {
    "nome": "herick",
    "senha": "123456"
}

var valor = JSON.stringify(objeto, mascara)

console.log(valor)
*/